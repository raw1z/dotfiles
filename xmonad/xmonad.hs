-- imports {{{
import Control.Arrow (first)
import Control.Monad (liftM2)
import qualified Data.Map as M
import System.IO (hPutStrLn)

import XMonad

import XMonad.Actions.CopyWindow (kill1)
import XMonad.Actions.CycleWS
import XMonad.Actions.Promote
import XMonad.Actions.RotSlaves (rotSlavesDown, rotAllDown)
import qualified XMonad.Actions.Search as S
import XMonad.Actions.Submap
import XMonad.Actions.WindowGo (raise)
import XMonad.Actions.WithAll (killAll)

import XMonad.Layout
import XMonad.Layout.NoBorders
import XMonad.Layout.Grid
import XMonad.Layout.Renamed (renamed, Rename(Replace))
import XMonad.Layout.ResizableTile
import XMonad.Layout.SimplestFloat
import XMonad.Layout.Spacing
import XMonad.Layout.Spiral (spiral)
import XMonad.Layout.ThreeColumns

import XMonad.Hooks.DynamicLog (dynamicLogWithPP, pad, wrap, xmobarPP, xmobarColor, shorten, PP(..))
import XMonad.Hooks.ManageDocks (avoidStruts, docks)

import XMonad.Prompt
import XMonad.Prompt.FuzzyMatch
import XMonad.Prompt.Man
import XMonad.Prompt.Shell (shellPrompt, safePrompt)
import XMonad.Prompt.Ssh
import XMonad.Prompt.Window (windowPrompt, allWindows, WindowPrompt(..))

import qualified XMonad.StackSet as W

import XMonad.Util.EZConfig (additionalKeysP)
import XMonad.Util.Run (spawnPipe)
import XMonad.Util.SpawnOnce (spawnOnce)
-- }}}
-- colors {{{
barBgColor :: String
barBgColor = "#0b1c2c"

barFgColor :: String
barFgColor = "#627e99"

currentColor :: String
currentColor = "#bf8b56"

layoutColor :: String
layoutColor = "#56bf8b"

titleColor :: String
titleColor = "#cbd6e2"

hiddenWithWindowsColor :: String
hiddenWithWindowsColor = "#568bbf"

urgentColor :: String
urgentColor = "#8b56bf"

windowCountColor :: String
windowCountColor = "#8bbf56"
-- }}}
-- variables {{{
myTerminal :: String
myTerminal = "alacritty"

myModMask :: KeyMask
myModMask = mod4Mask

myBorderWidth :: Dimension
myBorderWidth = 4

myNormalBorderColor :: String
myNormalBorderColor = barBgColor

myFocusedBorderColor :: String
myFocusedBorderColor = "#bf568b"

myWorkspaces :: [String]
myWorkspaces = ["I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"]

myStartupHook :: X ()
myStartupHook = do
    spawnOnce "nitrogen --restore &"
    spawnOnce "picom &"
-- }}}
-- prompts {{{
dtXPKeymap :: M.Map (KeyMask,KeySym) (XP ())
dtXPKeymap = M.fromList $
     map (first $ (,) controlMask)   -- control + <key>
     [ (xK_z, killBefore)            -- kill line backwards
     , (xK_k, killAfter)             -- kill line forwards
     , (xK_a, startOfLine)           -- move to the beginning of the line
     , (xK_e, endOfLine)             -- move to the end of the line
     , (xK_b, moveCursor Prev)       -- move cursor forward
     , (xK_f, moveCursor Next)       -- move cursor backward
     , (xK_w, killWord Prev) -- kill the previous word
     , (xK_v, pasteString)           -- paste a string
     , (xK_x, quit)                  -- quit out of prompt
     , (xK_m, setSuccess True >> setDone True)
     , (xK_h, deleteString Prev)
     , (xK_d, deleteString Next)
     , (xK_u, killBefore)
     ]
     ++
     map (first $ (,) altMask)       -- meta key + <key>
     [ (xK_BackSpace, killWord Prev) -- kill the prev word
     , (xK_f, moveWord Next)         -- move a word forward
     , (xK_b, moveWord Prev)         -- move a word backward
     , (xK_d, killWord Next)         -- kill the next word
     , (xK_n, moveHistory W.focusUp')   -- move up thru history
     , (xK_p, moveHistory W.focusDown') -- move down thru history
     ]
     ++
     map (first $ (,) 0)
     [ (xK_Return, setSuccess True >> setDone True)
     , (xK_KP_Enter, setSuccess True >> setDone True)
     , (xK_BackSpace, deleteString Prev)
     , (xK_Delete, deleteString Next)
     , (xK_Left, moveCursor Prev)
     , (xK_Right, moveCursor Next)
     , (xK_Home, startOfLine)
     , (xK_End, endOfLine)
     , (xK_Down, moveHistory W.focusUp')
     , (xK_Up, moveHistory W.focusDown')
     , (xK_Escape, quit)
     ]
     where altMask = mod1Mask


dtXPConfig :: XPConfig
dtXPConfig = def
      { font                = "xft:Iosevka Nerd Font:size=7:style=Bold:antialias=true:hinting=true"
      , bgColor             = barBgColor
      , fgColor             = barFgColor
      , borderColor         = barBgColor
      , promptBorderWidth   = 0
      , promptKeymap        = dtXPKeymap
      , position            = Top
      , height              = 28
      , historySize         = 256
      , historyFilter       = id
      , defaultText         = []
      , showCompletionOnTab = False
      -- , searchPredicate     = isPrefixOf
      , searchPredicate     = fuzzyMatch
      , alwaysHighlight     = True
      , maxComplRows        = Nothing      -- set to Just 5 for 5 rows
      }
-- }}}
-- layoutHook {{{
myLayoutHook = avoidStruts $ layouts
             where layouts       = tall
                                 ||| spirals
                                 ||| mirrorTall
                                 ||| grid
                                 ||| threeCols
                                 ||| threeRows
                                 ||| full
                                 ||| floats
                   resizableTall = ResizableTall 1 (3/100) (1/2) []
                   tall          = renamed [Replace "tall"]
                                   $ mySpacing
                                   $ resizableTall
                   mirrorTall    = renamed [Replace "mirror-tall"]
                                   $ mySpacing
                                   $ Mirror resizableTall
                   spirals       = renamed [Replace "spiral"]
                                   $ mySpacing
                                   $ spiral (6/7)
                   grid          = renamed [Replace "grid"]
                                   $ mySpacing
                                   $ Grid
                   full          = renamed [Replace "monocle"]
                                   $ noBorders
                                   $ Full
                   threeCols     = renamed [Replace "3-cols"]
                                   $ mySpacing
                                   $ ThreeCol 1 (3/100) (1/3)
                   threeRows     = renamed [Replace "3-rows"]
                                   $ mySpacing
                                   $ Mirror
                                   $ ThreeCol 1 (3/100) (1/3)
                   floats        = renamed [Replace "floats"]
                                   $ simplestFloat
                   mySpacing     = spacingRaw False screenBorder True windowBorder True
                   layoutSpacing = 5
                   screenBorder  = Border layoutSpacing layoutSpacing layoutSpacing layoutSpacing
                   windowBorder  = Border layoutSpacing layoutSpacing layoutSpacing layoutSpacing
-- }}}
-- Keys {{{
myKeys :: [(String, X ())]
myKeys =
    [ ("M-<Return>", spawn myTerminal)
    , ("M-S-<Return>", spawn "vivaldi-stable")
    , ("M-C-r", spawn "xmonad --recompile")
    , ("M-r", spawn "xmonad --recompile; xmonad --restart")

    -- prompts
    , ("M-s", shellPrompt dtXPConfig)
    , ("M-<Space>"
      , S.promptSearchBrowser dtXPConfig { maxComplRows = Just 0, historySize = 0 } "vivaldi-stable" S.duckduckgo
        >> raise (className =? "Vivaldi-stable")
      )
    , ("M-w", windowPrompt dtXPConfig Goto allWindows)

    -- windows
    , ("M-m", windows W.focusMaster)
    , ("M-<Backspace>", promote)
    , ("M-C-l", rotSlavesDown)
    , ("M-C-h", rotAllDown)
    , ("M-q", kill1)
    , ("M-C-q", killAll)

    -- layouts
    , ("M-<Left>", sendMessage Shrink)
    , ("M-<Right>", sendMessage Expand)
    , ("M-<Up>", sendMessage MirrorShrink)
    , ("M-<Down>", sendMessage MirrorExpand)
    , ("M-c", sendMessage NextLayout)
    , ("M-S-c", sendMessage FirstLayout)


    -- workspaces
    , ("M-h", prevWS)
    , ("M-l", nextWS)
    , ("M-C-k", shiftToPrev >> prevWS)
    , ("M-C-j", shiftToNext >> nextWS)
    , ("M-S-k", shiftToPrev)
    , ("M-S-j", shiftToNext)
    , ("M-C-<Space>", toggleWS)
    , ("<F1>", windows $ W.greedyView "1")
    , ("<F2>", windows $ W.greedyView "2")
    , ("<F3>", windows $ W.greedyView "3")
    , ("<F4>", windows $ W.greedyView "4")
    , ("<F5>", windows $ W.greedyView "5")
    , ("<F6>", windows $ W.greedyView "6")
    , ("<F7>", windows $ W.greedyView "7")
    , ("<F8>", windows $ W.greedyView "8")
    , ("<F9>", windows $ W.greedyView "9")
    ]
-- }}}
-- manageHook {{{
myManageHook = composeAll
    [ className =? "Nitrogen" --> viewShift ( myWorkspaces !! 8 )
    , className =? "Nitrogen" --> doFloat
    , (className =? "firefox" <&&> resource =? "Dialog") --> doFloat
    ]
    where viewShift = doF . liftM2 (.) W.greedyView W.shift
-- }}}
-- logHook {{{

-- wrap workspace tag
wrapWs = pad

-- currently focused workspace
rzCurrent = xmobarColor currentColor barBgColor . wrapWs

-- hidden workspaces which contain windows
rzHidden = xmobarColor hiddenWithWindowsColor barBgColor . wrapWs

-- hidden workspaces without window
rzHiddenNoWindows = xmobarColor barFgColor barBgColor . wrapWs

-- urgent workspace
rzUrgent = xmobarColor urgentColor barBgColor . wrapWs

-- current window title
rzTitle = wrapWs . xmobarColor titleColor barBgColor . shorten 60

-- current layout
rzLayout = xmobarColor layoutColor barBgColor . wrapWs

-- extras
rzExtras = [windowCount]
    where
        windowCount = gets $ Just . xmobarColor windowCountColor barBgColor . wrap "[" "]" . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset

-- separator between different log sections
rzSep = " "

-- separator between workspace tags
rzWsSep = ""

-- different sections order
rzOrder (ws:l:t:ex) = [ws,l] ++ ex ++ [t]

myLogHook xmproc = dynamicLogWithPP xmobarPP
        { ppOutput = hPutStrLn xmproc
        , ppCurrent = rzCurrent
        , ppHidden = rzHidden
        , ppHiddenNoWindows = rzHiddenNoWindows
        , ppLayout = rzLayout
        , ppTitle = rzTitle
        , ppSep =  rzSep
        , ppWsSep = rzWsSep
        , ppUrgent = rzUrgent
        , ppOrder = rzOrder
        , ppExtras = rzExtras
        }
-- }}}
-- main {{{
main = do
    xmproc0 <- spawnPipe "xmobar -x 0 /home/rz/.config/xmobar/xmobarrc"
    xmonad $ docks def
        { terminal = myTerminal
        , modMask = myModMask
        , borderWidth = myBorderWidth
        , workspaces = myWorkspaces
        , normalBorderColor = myNormalBorderColor
        , focusedBorderColor = myFocusedBorderColor
        , clickJustFocuses = False
        , startupHook = myStartupHook
        , layoutHook = myLayoutHook
        , manageHook = myManageHook
        , logHook = myLogHook xmproc0
        } `additionalKeysP` myKeys
-- }}}
