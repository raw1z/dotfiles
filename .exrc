" set foldmethod for all files opened in this instance
autocmd BufNewFile,BufRead * setlocal foldmethod=marker

" Xmonad {{{
fun! EditXmonad()
  if bufname("%") == ""
    edit xmonad/xmonad.hs
  else
    tabnew xmonad/xmonad.hs
  endif
endf

fun! ShowXmonadErrors()
  let errors_file = "xmonad/xmonad.errors"
  if filereadable(errors_file)
    let contents = readfile(errors_file)
    if len(contents) > 0
      exe "belowright new " . errors_file
    else
      echom "compilation successful"
    endif
  endif
endf

fun! SetupXmonad()
  nmap <buffer> <leader>e :silent call ShowXmonadErrors()<CR>
  nmap <buffer> <leader>r :silent !killall xmobar<CR>
  nmap <buffer> <leader>x :tabclose<CR>
endf

fun! SetupXmonadErrors()
  setlocal autoread
  nmap <buffer> <leader>r :silent !killall xmobar<CR>
  nmap <buffer> q :tabclose<CR>
  nmap <buffer> <leader>= :wincmd k<CR>
endf

command! Xmonad :call EditXmonad()
autocmd BufNewFile,BufRead xmonad/xmonad.hs call SetupXmonad()
autocmd BufNewFile,BufRead xmonad/xmonad.errors call SetupXmonadErrors()
autocmd BufNewFile,BufRead config/xmobar/xmobarrc setlocal ft=haskell

"}}}
