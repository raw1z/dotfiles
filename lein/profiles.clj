{
 :user {:dependencies [[com.bhauman/rebel-readline "0.1.4"]
                       [pjstadig/humane-test-output "0.8.3"]]
        :injections [(require 'pjstadig.humane-test-output)
                     (pjstadig.humane-test-output/activate!)]
        :aliases {"rebl" ["trampoline" "run" "-m" "rebel-readline.main"]}
        :plugins [[com.jakemccrary/lein-test-refresh "0.24.1"]
                  [cider/cider-nrepl "0.21.1"]]}
}
